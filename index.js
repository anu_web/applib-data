import {
  createBroadcast, createFetcher, createRetriever,
} from './elements';

export { createDataCache } from './elements';

export function createFeed({
  remote,
  cache = null,
  initial = null,
  interval = null,
  final = null,
  refreshTimeout = 500,
  retryRemoteWait = 0,
}) {
  // Create feed elements.
  const broadcast = createBroadcast();
  const {
    retrieveCache, retrieveUpdate,
  } = createRetriever(
    createFetcher(remote, cache, initial),
    broadcast,
    interval,
    retryRemoteWait,
  );

  function isPastFinal() {
    return final && Date.now() > final;
  }

  function willRefresh() {
    // Only refresh if there are still subscribers and time is not past final.
    return broadcast.hasSubscribers() && !isPastFinal();
  }

  async function refresh() {
    // Update if cache no longer exists (or valid).
    const cached = await retrieveCache();
    if (cached === null) {
      await retrieveUpdate();
    }

    if (willRefresh()) {
      setTimeout(refresh, refreshTimeout);
    }
  }

  // Get data.
  async function get() {
    // Retrieve cached data if exists.
    const data = await retrieveCache();
    if (data) {
      return data;
    }

    // Update if no cached data.
    return retrieveUpdate();
  }

  // Get update data.
  async function update() {
    return retrieveUpdate(true);
  }

  // Add subscriber.
  function subscribe(subscriber) {
    broadcast.addSubscriber(subscriber);
    return refresh();
  }

  // Remove subscriber.
  function unsubscribe(subscriber) {
    broadcast.removeSubscriber(subscriber);
  }

  return {
    get, update, subscribe, unsubscribe,
  };
}

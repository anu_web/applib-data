
export function createBroadcast() {
  const broadcast = {};
  let counter = 0;
  let lastUpdate;

  function sendSafely(subscriber, data) {
    // Catch subscriber errors.
    try {
      subscriber(data);
    } catch (e) {
      // TODO Log error.
      console.warn(e); // eslint-disable-line no-console
    }
  }

  function addSubscriber(subscriber) {
    const exists = Object.values(broadcast).some(s => s === subscriber);
    if (!exists) {
      broadcast[counter] = subscriber;
      counter += 1;

      if (lastUpdate !== undefined) {
        sendSafely(subscriber, lastUpdate);
      }
    } else {
      throw new Error('Subscriber already exists.');
    }
  }

  function removeSubscriber(subscriber) {
    const removed = Object.entries(broadcast).some(([i, s]) => {
      if (s === subscriber) {
        delete broadcast[i];
        return true;
      }
      return false;
    });

    if (!removed) {
      throw new Error('Subscriber to remove is not found.');
    }
  }

  function hasSubscribers() {
    return !!Object.keys(broadcast).length;
  }

  function send(update) {
    lastUpdate = update;

    Object.values(broadcast).forEach((s) => {
      sendSafely(s, update);
    });
  }

  return {
    addSubscriber, removeSubscriber, hasSubscribers, send,
  };
}


export function createDataCache(storage, key) {
  // Transparently wrap both sync/async storage API in data cache interface.
  const loadItem = async () => storage.getItem(key);
  const saveItem = async data => storage.setItem(key, data);

  // Transparently handle JSON.
  const load = async () => loadItem().then(value => JSON.parse(value));
  const save = async value => saveItem(JSON.stringify(value));

  return { load, save };
}


export function createFetcher(remote, cache, initial) {
  if (!remote) {
    throw new Error('Feed requires callable remote fetcher.');
  }

  if (cache && !(cache.load && cache.save)) {
    throw new Error(
      'Data cache must support asynchronous load and save, such as with createDataCache().',
    );
  }

  let cachedData;
  async function loadCache() {
    if (cachedData === undefined) {
      if (cache) {
        cachedData = await cache.load();
      } else {
        cachedData = null;
      }
    }

    return cachedData;
  }

  async function saveCache(dataUpdate) {
    cachedData = dataUpdate;

    if (cache) {
      await cache.save(dataUpdate);
    }
  }

  async function fetchAndSave(backend, lastUpdated = null) {
    const result = await backend();
    if (result !== null && result !== undefined) {
      const dataUpdate = { data: result, lastUpdated: lastUpdated || Date.now() };
      const saved = saveCache(dataUpdate);

      await saved;
      return dataUpdate;
    }

    return null;
  }

  async function fetchCache() {
    const cached = await loadCache();
    // Cast undefined to null.
    return cached === undefined ? null : cached;
  }

  async function fetchRemote() {
    // Use remote data fetcher.
    return fetchAndSave(remote);
  }

  async function fetchInitial() {
    // Use initial data fetcher.
    if (initial) {
      return fetchAndSave(initial, -1);
    }

    return null;
  }

  return {
    fetchCache, fetchRemote, fetchInitial,
  };
}


export function createRetriever(
  fetcher, broadcast, interval, retryRemoteWait = 0,
) {
  const {
    fetchCache, fetchRemote, fetchInitial,
  } = fetcher;

  function isExpired(lastUpdated) {
    return lastUpdated && !!interval && Date.now() >= lastUpdated + interval;
  }

  async function fetchSafely(fetch) {
    try {
      const fetched = await fetch();
      if (fetched !== null) {
        return fetched;
      }
    } catch (e) {
      // TODO Log error.
      console.warn(e); // eslint-disable-line no-console
    }

    return null;
  }

  function broadcastUpdate(data) {
    if (broadcast) {
      broadcast.send(data);
    }
  }

  async function retrieveCache() {
    // Attempt to fetch cached data (and load last updated time).
    const fetched = await fetchSafely(fetchCache);
    if (fetched !== null) {
      // Return data only if not expired.
      const { data, lastUpdated } = fetched;
      return isExpired(lastUpdated) ? null : data;
    }

    return null;
  }

  let lastFailedRemote = 0;
  const fetchUpdate = async (skipWaiting = false) => {
    // Attempt to fetch remote data, then cached data (even if expired), then initial data.
    // Broadcast remote and initial fetches.

    // Fetch remote only after failure retry wait.
    if (skipWaiting || Date.now() >= lastFailedRemote + retryRemoteWait) {
      const resultRemote = await fetchSafely(fetchRemote);
      if (resultRemote !== null) {
        // Reset last failed.
        lastFailedRemote = 0;

        const { data } = resultRemote;
        broadcastUpdate(data);
        return data;
      }

      lastFailedRemote = Date.now();
    }

    const resultCache = await fetchSafely(fetchCache);
    if (resultCache !== null) {
      const { data } = resultCache;
      return data;
    }

    const resultInitial = await fetchSafely(fetchInitial);
    if (resultInitial !== null) {
      const { data } = resultInitial;
      broadcastUpdate(data);
      return data;
    }

    return null;
  };

  // Group fetches.
  let pendingFetch = null;
  const retrieveUpdate = (skipWaiting = false) => {
    if (pendingFetch === null) {
      pendingFetch = fetchUpdate(skipWaiting);
      return Promise.resolve(pendingFetch).then((result) => {
        // Reset pending fetch before resolving.
        pendingFetch = null;
        return result;
      });
    }

    return Promise.resolve(pendingFetch);
  };

  return {
    retrieveCache, retrieveUpdate,
  };
}

import { createDataCache, createFetcher, createRetriever } from '../elements';

const {
  createBroadcast,
} = require('../elements');


jest.useRealTimers();

describe('broadcast', () => {
  it('adds and removes subscribers', () => {
    const broadcast = createBroadcast();
    expect(broadcast.hasSubscribers()).toBe(false);

    const s1 = jest.fn();
    const s2 = jest.fn();
    broadcast.addSubscriber(s1);
    expect(broadcast.hasSubscribers()).toBe(true);
    broadcast.addSubscriber(s2);
    expect(broadcast.hasSubscribers()).toBe(true);

    broadcast.removeSubscriber(s1);
    broadcast.removeSubscriber(s2);
    expect(broadcast.hasSubscribers()).toBe(false);
  });

  it('sends updates to subscribers', () => {
    const broadcast = createBroadcast();
    const s = jest.fn();
    broadcast.addSubscriber(s);
    broadcast.send('test value');

    expect(s).toHaveBeenCalledTimes(1);
    expect(s).toHaveBeenLastCalledWith('test value');
  });

  it('does not send updates to removed subscribers', () => {
    const broadcast = createBroadcast();
    const s1 = jest.fn();
    const s2 = jest.fn();
    broadcast.addSubscriber(s1);
    broadcast.addSubscriber(s2);
    broadcast.send('a');
    broadcast.removeSubscriber(s1);
    broadcast.send('b');

    expect(s1).toHaveBeenCalledTimes(1);
    expect(s1).toHaveBeenLastCalledWith('a');
    expect(s2).toHaveBeenCalledTimes(2);
    expect(s2).toHaveBeenNthCalledWith(1, 'a');
    expect(s2).toHaveBeenNthCalledWith(2, 'b');
  });

  it('sends last update to new subscriber', () => {
    const broadcast = createBroadcast();
    const s1 = jest.fn();
    const s2 = jest.fn();
    const s3 = jest.fn();

    broadcast.addSubscriber(s1);
    expect(s1).toHaveBeenCalledTimes(0);

    broadcast.send('test value');
    expect(s1).toHaveBeenCalledTimes(1);
    broadcast.addSubscriber(s2);
    expect(s2).toHaveBeenCalledTimes(1);
    expect(s2).toHaveBeenLastCalledWith('test value');

    broadcast.removeSubscriber(s1);
    broadcast.removeSubscriber(s2);
    expect(broadcast.hasSubscribers()).toBe(false);
    broadcast.send('new value');
    broadcast.addSubscriber(s3);
    expect(s3).toHaveBeenCalledTimes(1);
    expect(s3).toHaveBeenLastCalledWith('new value');
  });

  it('aborts on adding duplicate subscriber', () => {
    const broadcast = createBroadcast();
    const s = jest.fn();
    broadcast.addSubscriber(s);
    expect(() => {
      broadcast.addSubscriber(s);
    }).toThrowError();
  });

  it('aborts on removing unknown subscriber', () => {
    const broadcast = createBroadcast();
    broadcast.addSubscriber(() => {});
    expect(broadcast.hasSubscribers()).toBe(true);
    expect(() => {
      broadcast.removeSubscriber(() => {});
    }).toThrowError();
  });
});

describe('data cache', () => {
  it('wraps synchronous storage interface', async () => {
    const storage = {
      getItem: jest.fn().mockReturnValue('"v"'),
      setItem: jest.fn(),
    };
    const cache = createDataCache(storage, 'k');

    expect(await cache.load()).toBe('v');
    expect(storage.getItem).toHaveBeenCalledTimes(1);
    expect(storage.getItem).toHaveBeenLastCalledWith('k');

    await cache.save('w');
    expect(storage.setItem).toHaveBeenCalledTimes(1);
    expect(storage.setItem).toHaveBeenLastCalledWith('k', '"w"');
  });

  it('wraps asynchronous storage interface', async () => {
    const storage = {
      getItem: jest.fn().mockResolvedValue('"v"'),
      setItem: jest.fn().mockImplementation(async () => {}),
    };
    const cache = createDataCache(storage, 'k');

    expect(await cache.load()).toBe('v');
    expect(storage.getItem).toHaveBeenCalledTimes(1);
    expect(storage.getItem).toHaveBeenLastCalledWith('k');

    await cache.save('w');
    expect(storage.setItem).toHaveBeenCalledTimes(1);
    expect(storage.setItem).toHaveBeenLastCalledWith('k', '"w"');
  });
});

describe('fetcher', () => {
  it('requires remote', () => {
    expect(() => {
      createFetcher();
    }).toThrowError();
  });

  it('fetches remote data and defaults cache to null until fetch and initial to null', async () => {
    const fetcher = createFetcher(async () => 'remote');
    expect(await fetcher.fetchCache()).toBe(null);
    expect(await fetcher.fetchInitial()).toBe(null);
    const expectedRemote = expect.objectContaining({
      data: 'remote',
      lastUpdated: expect.any(Number),
    });
    expect(await fetcher.fetchRemote()).toEqual(expectedRemote);
    expect(await fetcher.fetchCache()).toEqual(expectedRemote);
    expect(await fetcher.fetchInitial()).toBe(null);
  });

  it('requires valid data cache if provided', () => {
    expect(() => {
      createFetcher(() => {}, 'invalid cache');
    }).toThrowError();
  });

  it('fetches cached data', async () => {
    const fetcher = createFetcher(() => {}, {
      load: jest.fn().mockResolvedValue('verbatim'),
      save: jest.fn(),
    });
    expect(await fetcher.fetchCache()).toBe('verbatim');
  });

  it('saves fetched remote data to cache and to memory', async () => {
    let i = 0;
    const remote = async () => {
      i += 1;
      return `remote ${i}`;
    };
    const saveCache = jest.fn().mockImplementation(async () => {});
    const fetcher = createFetcher(remote, {
      load: jest.fn(),
      save: saveCache,
    });

    await fetcher.fetchRemote();
    const expectedRemote1 = expect.objectContaining({
      data: 'remote 1',
      lastUpdated: expect.any(Number),
    });
    expect(saveCache).toHaveBeenCalledTimes(1);
    expect(saveCache).toHaveBeenLastCalledWith(expectedRemote1);
    expect(await fetcher.fetchCache()).toEqual(expectedRemote1);

    await fetcher.fetchRemote();
    const expectedRemote2 = expect.objectContaining({
      data: 'remote 2',
      lastUpdated: expect.any(Number),
    });
    expect(saveCache).toHaveBeenCalledTimes(2);
    expect(saveCache).toHaveBeenLastCalledWith(expectedRemote2);
    expect(await fetcher.fetchCache()).toEqual(expectedRemote2);
  });

  it('fetches initial data and saves to cache', async () => {
    const cache = {
      load: jest.fn().mockResolvedValue(null),
      save: jest.fn().mockImplementation(async () => {}),
    };
    const fetcher = createFetcher(
      () => {},
      cache,
      async () => 'initial',
    );

    expect(await fetcher.fetchInitial()).toEqual(expect.objectContaining({
      data: 'initial',
      lastUpdated: -1,
    }));
    expect(cache.save).toHaveBeenCalledTimes(1);
    expect(cache.save).toHaveBeenLastCalledWith(expect.objectContaining({
      data: 'initial',
      lastUpdated: -1,
    }));
  });
});

describe('retriever', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
    jest.spyOn(global.console, 'warn').mockImplementation(() => {});
  });

  const createCacheStub = (data, lastUpdated) => ({
    load: async () => ({ data, lastUpdated }),
    save: async () => {},
  });

  it('retrieves cache if not expired', async () => {
    expect(await createRetriever(
      createFetcher(
        async () => 'remote',
        createCacheStub('cache', Infinity),
      ),
    ).retrieveCache()).toBe('cache');

    expect(await createRetriever(
      createFetcher(
        async () => 'remote',
        createCacheStub('cache', -1),
      ),
    ).retrieveCache()).toBe('cache');

    expect(await createRetriever(
      createFetcher(
        async () => 'remote',
        createCacheStub('cache', -1),
      ),
      null,
      1,
    ).retrieveCache()).toBe(null);
  });

  it('fetches from remote only once for multiple retrievals', async () => {
    const remote = jest.fn(async () => 'remote');
    const retriever = createRetriever(
      createFetcher(remote),
    );

    await Promise.all([
      retriever.retrieveUpdate(),
      retriever.retrieveUpdate(),
      retriever.retrieveUpdate(),
    ]);

    expect(remote).toHaveBeenCalledTimes(1);
  });

  it('retrieves initial data with optional cache', async () => {
    const cache = {
      load: jest.fn(),
      save: jest.fn(),
    };
    const retriever = createRetriever(createFetcher(
      () => {},
      cache,
      async () => 'initial',
    ));

    expect(await retriever.retrieveCache()).toBe(null);
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(cache.save).toHaveBeenCalledTimes(1);
    expect(await retriever.retrieveCache()).toBe('initial');

    // Repeat.
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(cache.save).toHaveBeenCalledTimes(1);
  });

  it('retrieves initial data if cache throws error', async () => {
    const cache = {
      load: jest.fn().mockRejectedValue(new Error()),
      save: jest.fn().mockImplementation(async () => {}),
    };
    const retriever = createRetriever(createFetcher(
      () => {},
      cache,
      async () => 'initial',
    ));

    expect(await retriever.retrieveUpdate()).toEqual('initial');
  });

  it('retrieves update from remote then with initial if both update and cache failed', async () => {
    const cache = {
      load: jest.fn().mockRejectedValue(new Error('Intentional cache fail for testing')),
      save: jest.fn(),
    };
    const retriever = createRetriever(createFetcher(
      jest.fn()
        .mockRejectedValueOnce(new Error('Intentional fail for testing'))
        .mockResolvedValueOnce('remote')
        .mockRejectedValueOnce(new Error('Intentional fail for testing'))
        .mockResolvedValueOnce('remote 2'),
      cache,
      async () => 'initial',
    ));
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(cache.save).toHaveBeenLastCalledWith(expect.objectContaining({
      data: 'initial',
      lastUpdated: expect.any(Number),
    }));
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(await retriever.retrieveUpdate()).toBe('remote 2');
  });

  it('waits before retrying remote if specified unless waiting skipped or last successful', async () => {
    const remote = jest.fn()
      .mockRejectedValueOnce(new Error('Remote fetch error'))
      .mockResolvedValueOnce('remote')
      .mockRejectedValueOnce(new Error('Remote fetch error'))
      .mockResolvedValueOnce('remote')
      .mockResolvedValueOnce('remote')
    const retriever = createRetriever(
      createFetcher(
        remote,
        null,
        async () => 'initial',
      ),
      null,
      10,
      500,
    );

    // Fail.
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(remote).toHaveBeenCalledTimes(1);
    // Return cache immediately within waiting period.
    await new Promise(r => setTimeout(r, 10));
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(remote).toHaveBeenCalledTimes(1);

    // Fetch when past waiting period.
    await new Promise(r => setTimeout(r, 500));
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(remote).toHaveBeenCalledTimes(2);
    // Fail and return cache immediately again.
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(remote).toHaveBeenCalledTimes(3);
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(remote).toHaveBeenCalledTimes(3);
    // Skip waiting with forced fetch.
    expect(await retriever.retrieveUpdate(true)).toBe('remote');
    expect(remote).toHaveBeenCalledTimes(4);
    // Implicitly skip waiting if past interval and last successful, even within waiting period.
    await new Promise(r => setTimeout(r, 10));
    expect(await retriever.retrieveUpdate()).toBe('remote');
    expect(remote).toHaveBeenCalledTimes(5);
  });

  it('retrieves null if fetch failed', async () => {
    const retriever = createRetriever(createFetcher(
      jest.fn().mockRejectedValue(new Error('Remote fetch error')),
      {
        load: jest.fn(),
        save: jest.fn(),
      },
      jest.fn().mockRejectedValue(new Error('Initial fetch error')),
    ));

    expect(await retriever.retrieveUpdate()).toBe(null);
    expect(await retriever.retrieveCache()).toBe(null);
  });

  it('retrieves expired cache if update failed', async () => {
    expect(await createRetriever(
      createFetcher(
        jest.fn().mockRejectedValueOnce(new Error('Intentional fail for testing')),
        createCacheStub('cache', -1),
      ),
      null,
      1,
    ).retrieveUpdate()).toBe('cache');
  });

  it('retrieves last fetched cache if cache fails', async () => {
    const cache = {
      load: jest.fn()
        .mockResolvedValueOnce({ data: 'cache', lastUpdated: -1 })
        .mockRejectedValueOnce(new Error('Intentional cache fail for testing')),
      save: jest.fn(),
    };
    const retriever = createRetriever(
      createFetcher(
        async () => 'remote',
        cache,
      ),
    );

    expect(await retriever.retrieveCache()).toBe('cache');
    expect(await retriever.retrieveCache()).toBe('cache');
  });

  it('retrieves expired cache on failed update even if initial data available', async () => {
    const retriever = createRetriever(
      createFetcher(
        jest.fn().mockRejectedValue(new Error('Intentional fail for testing')),
        createCacheStub('cache', -1),
        async () => 'initial',
      ),
      null,
      1,
    );

    expect(await retriever.retrieveUpdate()).toBe('cache');
    expect(await retriever.retrieveUpdate()).toBe('cache');
  });

  it('fetches initial data only once if update failed', async () => {
    const initial = jest.fn(async () => 'initial');
    const retriever = createRetriever(
      createFetcher(
        jest.fn().mockRejectedValueOnce(new Error('Intentional fail for testing')),
        null,
        initial,
      ),
    );

    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(initial).toHaveBeenCalledTimes(1);
  });

  it('retrieves initial data as expired', async () => {
    const remote = jest.fn().mockRejectedValue(new Error('Remote fetch error'));
    const retriever = createRetriever(createFetcher(
      remote,
      null,
      async () => 'initial',
    ));

    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(remote).toHaveBeenCalledTimes(1);
    expect(await retriever.retrieveUpdate()).toBe('initial');
    expect(remote).toHaveBeenCalledTimes(2);
  });

  it('broadcasts on updating data', async () => {
    const subscriber = jest.fn();
    const broadcast = createBroadcast();
    broadcast.addSubscriber(subscriber);

    const retriever = createRetriever(
      createFetcher(
        jest.fn().mockRejectedValueOnce(null).mockResolvedValueOnce('remote'),
        null,
        async () => 'initial',
      ),
      broadcast,
    );

    await retriever.retrieveUpdate();
    expect(subscriber).toHaveBeenCalledTimes(1);
    expect(subscriber).toHaveBeenLastCalledWith('initial');

    await retriever.retrieveUpdate();
    expect(subscriber).toHaveBeenCalledTimes(2);
    expect(subscriber).toHaveBeenLastCalledWith('remote');
  });
});

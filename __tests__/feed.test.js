import 'jest-localstorage-mock';
import { createFeed, createDataCache } from '..';

const fetch = require('jest-fetch-mock');

global.fetch = fetch;


describe('feed', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('requires remote', () => {
    expect(() => {
      createFeed({});
    }).toThrowError();
  });

  it('contains feed interface', () => {
    const feed = createFeed({
      remote: async () => {},
    });
    expect(feed).toEqual(expect.objectContaining({
      get: expect.any(Function),
      update: expect.any(Function),
      subscribe: expect.any(Function),
      unsubscribe: expect.any(Function),
    }));
  });

  it('returns fetched data', async () => {
    fetch.mockResponseOnce(JSON.stringify('fetched'));
    const feed = createFeed({
      remote: () => fetch().then(resp => resp.json()),
    });
    expect(await feed.get()).toBe('fetched');
  });

  it('caches fetches unless explicitly updating', async () => {
    const remote = jest.fn(async () => 'remote');
    const feed = createFeed({
      remote,
      interval: 200,
    });
    await feed.get();
    expect(remote).toHaveBeenCalledTimes(1);

    await feed.get();
    expect(remote).toHaveBeenCalledTimes(1);

    await new Promise(r => setTimeout(r, 200));
    await feed.get();
    expect(remote).toHaveBeenCalledTimes(2);

    await feed.update();
    expect(remote).toHaveBeenCalledTimes(3);
  });

  it('uses localStorage for cache', async () => {
    const cache = createDataCache(global.localStorage, 'test');
    jest.spyOn(cache, 'save');

    fetch.mockResponseOnce(JSON.stringify('fetched'));
    const feed = createFeed({
      remote: () => fetch().then(resp => resp.json()),
      cache,
    });
    await feed.get();

    const expected = expect.objectContaining({
      data: 'fetched',
      lastUpdated: expect.any(Number),
    });
    expect(JSON.parse(global.localStorage.__STORE__['test'])).toEqual(expected);
    expect(await feed.get()).toBe('fetched');
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(cache.save).toHaveBeenCalledTimes(1);
  });

  it('groups remote fetches', async () => {
    jest.spyOn(global.console, 'warn').mockImplementation(() => {});

    fetch.mockRejectOnce()
      .mockResponseOnce(JSON.stringify('fetched'));

    const remote = jest.fn(() => fetch().then(resp => resp.json()));
    const initial = jest.fn(async () => 'initial');
    const feed = createFeed({ remote, initial });
    await Promise.all([
      feed.update(),
      feed.update(),
      feed.update(),
    ]);
    expect(remote).toHaveBeenCalledTimes(1);
    expect(initial).toHaveBeenCalledTimes(1);

    await Promise.all([
      feed.update(),
      feed.update(),
      feed.update(),
    ]);
    expect(remote).toHaveBeenCalledTimes(2);
    expect(initial).toHaveBeenCalledTimes(1);
  });

  it('updates a new subscriber immediately if data is cached', async () => {
    const remote = jest.fn(async () => 'remote');
    const feed = createFeed({ remote });

    // Ensure feed is cached.
    await feed.get();
    expect(remote).toHaveBeenCalledTimes(1);
    await feed.get();
    expect(remote).toHaveBeenCalledTimes(1);

    // Attempt to call subscriber.
    const subscriber = jest.fn();
    try {
      feed.subscribe(subscriber);
      await new Promise(r => setImmediate(r));
      expect(subscriber).toHaveBeenCalledTimes(1);
    } finally {
      feed.unsubscribe(subscriber);
    }
  });

  it('refreshes only with subscribers', async () => {
    const remote = jest.fn(async () => 'remote');
    const feed = createFeed({
      remote,
      interval: 500,
      debounceTimeout: 0,
    });
    const subscriber = jest.fn();

    // Call once.
    await feed.update();
    expect(remote).toHaveBeenCalledTimes(1);

    // Check no new calls in background while cache expires.
    await new Promise(r => setTimeout(r, 1000));
    expect(remote).toHaveBeenCalledTimes(1);

    try {
      expect(subscriber).toHaveBeenCalledTimes(0);
      // Add subscriber to receive last update.
      const promise = feed.subscribe(subscriber);
      expect(subscriber).toHaveBeenCalledTimes(1);
      // Expect update to immediate occur upon subscription.
      await promise;
      expect(subscriber).toHaveBeenCalledTimes(2);
      expect(remote).toHaveBeenCalledTimes(2);
    } finally {
      feed.unsubscribe(subscriber);
    }

    // Wait for background refresh.
    await new Promise(r => setTimeout(r, 1000));
    expect(remote).toHaveBeenCalledTimes(3);
    expect(subscriber).toHaveBeenCalledTimes(2);
  });

  it('updates subscribers until final time', async () => {
    fetch.mockResponse('"fetched"');

    const remote = jest.fn(() => fetch().then(resp => resp.json()));
    const feed = createFeed({
      remote,
      interval: 500,
      final: Date.now() + 1500,
      debounceTimeout: 0,
    });
    const subscriber = jest.fn();

    // Call once.
    await feed.update();
    expect(remote).toHaveBeenCalledTimes(1);

    try {
      feed.subscribe(subscriber);

      await new Promise(r => setTimeout(r, 1000));
      const remoteCallsInitial = remote.mock.calls.length;
      expect(remoteCallsInitial).toBeGreaterThan(1);
      expect(subscriber).toHaveBeenCalledTimes(remoteCallsInitial);

      await new Promise(r => setTimeout(r, 1000));
      const remoteCallsFinal = remote.mock.calls.length;
      expect(remoteCallsFinal).toBeGreaterThan(remoteCallsInitial);
      expect(subscriber).toHaveBeenCalledTimes(remoteCallsFinal);

      await new Promise(r => setTimeout(r, 1000));
      expect(remote.mock.calls.length).toBe(remoteCallsFinal);
      expect(subscriber).toHaveBeenCalledTimes(remoteCallsFinal);
    } finally {
      feed.unsubscribe(subscriber);
    }
  });

  it('catches and redirects subscriber errors', async () => {
    jest.spyOn(global.console, 'warn').mockImplementation(() => {});

    const feed = createFeed({
      remote: async () => 'remote',
      interval: 1000,
    });

    expect(global.console.warn).toHaveBeenCalledTimes(0);
    const subscriber = jest.fn(() => { throw new Error('Subscriber error'); });

    try {
      feed.subscribe(subscriber);

      await new Promise(r => setTimeout(r, 100));

      expect(console.warn).toHaveBeenCalledTimes(1);
      expect(console.warn.mock.calls[0][0].toString()).toContain('Subscriber error');

      await new Promise(r => setTimeout(r, 1500));

      expect(console.warn).toHaveBeenCalledTimes(2);
      expect(console.warn.mock.calls[1][0].toString()).toContain('Subscriber error');
    } finally {
      feed.unsubscribe(subscriber);
    }
  });
});

// TODO Refactor all timer tests to deterministic.
